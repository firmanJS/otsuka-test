const arrayA = [
    {
        code: 'M', name: 'Manggo',
        code: 'A', name: 'Apple',
        code: 'Me', name: 'Mellon',
        code: 'O', name: 'Orange',
        code: 'P', name: 'Pinnaple',
    }
]

const arrayB = [
    {
        code: 'M', name: 'Manggo',
        code: 'P', name: 'Pinnaple',
        code: 'PE', name: 'Pears',
    }
]

function question_one(){
    return arrayA.concat(arrayB)
}

function question_two(){
    return [...arrayA, ...arrayB]    
}

function question_thre(){
    return [...arrayA, ...arrayB]    
}

const data = [
    {
        code: '2000091',
        name: 'PT. TULUS MAJU PANTANG MUNDUR'
    },{
        code: '2000091',
        name: 'PT. PANTANG MUNDUR TULUS MAJU'
    }
]

data.map(r => {
    r.customer = `${r.code} - ${r.name}`
    return r
})


console.log(question_one())
console.log(question_two())
console.log(question_thre())
console.log(`jawaban no 1 bagian 2 : ${data}`)

